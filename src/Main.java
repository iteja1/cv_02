public class Main {

    public static void main(String[] args) {
        String artitmetic = "2 + 3 * 4 * 5 * 6 - 2 / 6";

        Parser p = new Parser();

        String[] tokens = p.createTokens(artitmetic);

        System.out.println("Tokeny:");
        for (String token : tokens){
            System.out.println(token);
        }
        System.out.println("-------------------------------------------");
        System.out.printf("Infix: %s", artitmetic);
        String[] postFix = p.createPostfix(tokens);
        System.out.print("\nPostFix: ");
        for (String fix : postFix){
            System.out.print(fix + " ");
        }
        System.out.println("\n-------------------------------------------");
        System.out.printf("Calculate postfix: %f", p.calculatePostfix(postFix));

    }
}
