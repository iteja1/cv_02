import java.lang.management.OperatingSystemMXBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Parser {

    private HashMap<String, Integer> OPERATORS;

    public Parser() {

        OPERATORS = new HashMap<String, Integer>();
        OPERATORS.put("+", 1);
        OPERATORS.put("-", 2);
        OPERATORS.put("*", 3);
        OPERATORS.put("/", 4);
    }

    public String[] createTokens(String aritmetic) {
        return aritmetic.split(" ");
    }

    public String[] createPostfix(String[] infix) {
        ArrayList<String> out = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();

        for (String token : infix) {
            if (isOperator(token)) {
                while (!stack.empty() && isOperator(stack.peek())) {
                    if (comparePriority(token, stack.peek()) <= 0) {
                        out.add(stack.pop());
                        continue;
                    }
                    break;
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.empty() && !stack.peek().equals("(")) {
                    out.add(stack.pop());
                }
                stack.pop();
            } else {
                out.add(token);
            }
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        String[] output = new String[out.size()];
        return out.toArray(output);
    }

    private boolean isOperator(String token) {
        return OPERATORS.containsKey(token);
    }

    private int comparePriority(String token1, String token2) {
        if (!isOperator(token1) || !isOperator(token2)) {
            throw new IllegalArgumentException("Invalid!");
        }
        return OPERATORS.get(token1) - OPERATORS.get(token2);
    }

    public double calculatePostfix(String[] postFix) {
        Stack<String> stack = new Stack<String>();

        for (String token : postFix) {
            if (!isOperator(token)) {
                stack.push(token);
            } else {

                double d2 = Double.valueOf(stack.pop());
                double d1 = Double.valueOf(stack.pop());

                Double result = token.compareTo("+") == 0 ? d1 + d2 :
                        token.compareTo("-") == 0 ? d1 - d2 :
                                token.compareTo("*") == 0 ? d1 * d2 :
                                        d1 / d2;
                stack.push(String.valueOf(result));
            }
        }

        return Double.valueOf(stack.pop());
    }
}
